<?php

namespace Clearlink\InContact;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\HandlerStack;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

use Clearlink\InContact\Api\MethodInfo;

class InContact {

    private $config;

    private $token;
    private $token_expires;
    private $refresh_token;
    private $refresh_uri;

    private $stack = null;
    private $token_promise = null;
    static private $methods = [];

    public function __construct(Config $config){
        $this->config = $config;

        $this->stack = HandlerStack::create();
        $this->stack->push(Middleware::mapRequest(function (RequestInterface $r) {
            return $r->withHeader('Accept','application/json, text/javascript, */*; q=0.01');
        }));
        $this->stack->push(Middleware::mapRequest(function (RequestInterface $r) {
            if(isset($this->token))
                return $r->withHeader('Authorization', "bearer $this->token");
            
            return $r;
        }));

        $this->getToken();
    }

    public function __call($name, $arguments)
    {
        $use_async = strpos($name, 'Async') !== false;
        //Get method info
        if(!$method_info = $this->getMethodInfo($use_async ? substr($name, 0, -5) : $name))
            throw new \Exception("The requested method is not supported.", 1);
            
        //Ensure RequestParams is valid for the requested method
        $params = $arguments[0];
        if($errors = $method_info->validateParamaters($params))
            throw new \Exception("The supplied RequestParamaters are not valid for the request", 1);

        $request = new Request($method_info->method, "services/v$params->version.0".$method_info->generateRequestPath($params));
        $callback = function (ResponseInterface $res) use ($method_info) {
            return $this->requestResponseHandler($res, $method_info);
        };
        $request_options = $method_info->generateRequestOptions($params);

        if(isset($this->token_promise))
            $this->token_promise->wait();

        if(!$use_async)
            return $this->request($request, $this->client, $callback, $request_options);
        else {
            return $this->requestAsync($request, $this->client, $callback, $request_options);
        }
    }
    
    private function getMethodInfo($name)
    {
        if(!$method_info = self::$methods[$name]){
            $method_info = new MethodInfo($name);
            self::$methods[$name] = $method_info;
        }

        return $method_info;
    }

    private function request(Request $request, Client $client, $callback, Array $request_options = []){
        try {
            return $callback(
                $client->send($request, $request_options)
            );
        } catch (RequestException $e) {
            $this->responseExceptionHandler($e);
        }
    }

    private function requestAsync(Request $request, Client $client, $callback, Array $request_options = []){
        return $client->sendAsync($request, $request_options)
            ->then($callback, function (RequestException $e) {
                $this->responseExceptionHandler($e);
            });;
    }
    
    private function getToken()
    {
        $request_options = [
            'json' => [
                'grant_type' => 'password',
                'username' => $this->config->userName(),
                'password' => $this->config->password(),
                'scope' => $this->config->requestedScopes(true)
            ],
            'headers' => [
                'Authorization' => 'basic '.$this->config->basicAuthKey()
            ]
        ];
        $request = new Request('POST', 'https://api.incontact.com/InContactAuthorizationServer/Token');

        $this->token_promise = $this->requestAsync($request, 
            new Client(['handler' => $this->stack]), 
            function(ResponseInterface $res){
                $this->tokenResponseHandler($res);
            },
            $request_options);
    }

    private function requestResponseHandler(ResponseInterface $res, MethodInfo $method_info)
    {
        if($res->getStatusCode() == 204)
            return [];

        $temp = array_reduce($method_info->response_key, function($carry, $key){
            return $carry[$key];
        }, json_decode($res->getBody(), TRUE));

        return $temp ?: [];
    }

    private function tokenResponseHandler(ResponseInterface $res)
    {
        $token_response = json_decode($res->getBody(), TRUE);

        $this->token = $token_response['access_token'];
        $this->expires = time() + $token_response['expires_in'];
        $this->refresh_token = $token_response['refresh_token'];
        $this->refresh_uri = $token_response['refresh_token_server_uri'];

        $this->client = new Client([
            'base_uri' => $token_response['resource_server_base_uri'],
            'handler' => $this->stack
        ]);
    }

    private function responseExceptionHandler(RequestException $e)
    {
        //echo "An Error has occured\n";
        //This needs to check for a 401 and retrieve a new token then reissue the request
        throw $e;
    }
}