<?php

namespace Clearlink\InContact;

class Config {
    private $app_name;
    private $vendor;
    private $business_unit_number;
    private $user_name;
    private $password;
    private $scopes;

    private $basic_auth_key;

    public function __construct($app_name, $vendor, $business_unit_number, $user_name, $password, $scopes) {
        $this->app_name             = $app_name;
        $this->vendor               = $vendor;
        $this->business_unit_number = $business_unit_number;
        $this->user_name            = $user_name;
        $this->password             = $password;
        $this->scopes               = $scopes;
    }

    public function appName()
    {
        return $this->app_name;
    }

    public function vendor()
    {
        return $this->vendor;
    }

    public function businessUnitNumber()
    {
        return $this->business_unit_number;
    }

    public function userName()
    {
        return $this->user_name;
    }

    public function password()
    {
        return $this->password;
    }

    public function requestedScopes($as_string)
    {
        return $as_string 
            ? implode(" ", $this->scopes)
            : $this->scopes;
    }

    public function basicAuthKey()
    {
        return $this->basic_auth_key 
            ?: $basic_auth_key = base64_encode("$this->app_name@$this->vendor:$this->business_unit_number");
    }
}