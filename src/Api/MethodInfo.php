<?php

namespace Clearlink\InContact\Api;

use Clearlink\InContact\RequestParams;

class MethodInfo {

    public $path;
    public $method;
    public $minimum_version;
    public $maximum_version;
    public $response_key;
    public $paramaters = [];
    
    private $non_mapped_paramaters;
    private $required_paramaters = [];
    private $path_paramater_mapping = [];

    public function __construct($method_name)
    {
        $file_path = __DIR__."/MethodDefinitions/$method_name.json";
        if(!file_exists($file_path))
            throw new \Exception("Method $method_name is not supported");
        
        if(!$method_definition = json_decode(file_get_contents($file_path), TRUE))
            throw new \Exception("Could not decode the json for method $method_name", 1);

        array_walk($method_definition, function($value, $key){
            if($key != 'paramaters' && $key != 'response_key' && property_exists($this, $key))
                $this->$key = $value;
        });

        $this->response_key = explode('.',$method_definition['response_key']);

        if(isset($method_definition['paramaters']))
             $this->paramaters = array_reduce(
                array_map(function($paramater_desc){
                    return new Paramater($paramater_desc);
                }, $method_definition['paramaters']),
                function($carry, Paramater $paramater){
                    $carry[$paramater->name()] = $paramater;
                    return $carry;
                }, []);

        $this->required_paramaters = array_filter($this->paramaters, function (Paramater $paramater){
            return $paramater->isRequired();
        });

        if(empty($this->path_paramater_mapping))
            $this->non_mapped_paramaters = $this->paramaters;
        else
            $this->non_mapped_paramaters = array_filter($this->paramaters, function(Paramater $paramater){
                return !array_key_exists($paramater->name(), array_flip($this->path_paramater_mapping));
            });
    }

    public function validateParamaters(RequestParams $request_params)
    {
        //This needs to be implemented
    }

    public function generateRequestPath(RequestParams $request_params)
    {
        if(empty($this->path_paramater_mapping))
            return $this->path;

        $ret_path = $this->path;
        array_walk($this->path_paramater_mapping, function ($property, $key) use (&$ret_path, $request_params) {
            $ret_path = str_replace("{".$key."}", $request_params->$property, $ret_path);
        });
        return $ret_path;
    }

    public function generateRequestOptions(RequestParams $request_params)
    {
        $request_options = [];

        if(!empty($this->non_mapped_paramaters)){
            $request_options[($this->method == "GET" ? "query" : "json")] = array_reduce(
                array_map(function(Paramater $paramater) use ($request_params){
                    $value = $request_params->{$paramater->name()};
                    if( $value || ( $value === false && $paramater->type() == 'boolean' ))
                        return [$paramater->name(), $paramater->formatValue($value)];
                    
                    return [];
                }, $this->non_mapped_paramaters),
                function($carry, $item){
                    if(!empty($item))
                        $carry[$item[0]] = $item[1];

                    return $carry;
                },
                []);
        }

        return $request_options;
    }
}