<?php

namespace Clearlink\InContact\Api;

class Paramater {
    private $name;
    private $required;
    private $type;

    static private $typeFuncs;

    public function __construct($paramater_definition)
    {
        $this->name     = $paramater_definition['name'];
        $this->required = isset($paramater_definition['required']) && $paramater_definition['required'] == true;
        $this->type     = $paramater_definition['type'];

        if(!isset(self::$typeFuncs)){
            self::$typeFuncs = [
                'numeric' => function($value){
                    return (int)$value[0];
                },
                'list' => function($value){
                    return implode(",",$value[0]);
                },
                'boolean' => function($value){
                    return $value[0] === true ? 'true' : 'false';
                },
                'dateTime' => function($value){
                    return $value[0]->toIso8601String();
                },
                'text' => function($value){
                    return $value[0];
                }
            ];
        }
    }

    public function name()
    {
        return $this->name;
    }

    public function isRequired()
    {
        return $this->required;
    }

    public function type()
    {
        return $this->type;
    }

    public function formatValue($value)
    {
        return call_user_func(self::$typeFuncs[$this->type], [$value]);
    }
} 