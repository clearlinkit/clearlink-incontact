<?php

namespace Clearlink\InContact;

class RequestParams {
    private $params = [];

    static public $version;

    public function __get($name)
    {
        $ret = array_key_exists($name, $this->params) 
            ? $this->params[$name] 
            : null;

        if(!$ret && $name == 'version')
            $ret = self::$version;
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }
}